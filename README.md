# gwjax

Trying JAX to generate gravitational waves

conda create -n gwjax python=3.7
conda install -c conda-forge gwjax
# also installed numpyro
conda install -c conda-forge numpyro
# pycbc
conda install -c conda-forge pycbc
# jupyter
conda install -c conda-forge jupyterlab
